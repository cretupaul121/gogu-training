package exercitiu3;

public class TestBinarySearch {
	
	public int search(int[] arr,int left, int right, int element) {
		
		if(left == right && arr[left] == element) {
			return left;
		}
		
		while(left <= right) {
			int mid = (left + right ) / 2;
			
			if(element == arr[mid]) return mid;
			
			if(element < arr[mid]) {
				return search(arr,left,mid-1,element);
			}
			
			if(element > arr[mid]) {
				return search(arr,mid+1,right,element);
			}
			
		}
		
		return -1;
		
	}
	
	
	public static void main(String[] args) {
		
		TestBinarySearch test = new TestBinarySearch();
		
		int []arr = new int[] {1,5,7,8,9,10,20,24};
		
		
		int pos = test.search(arr,0,arr.length-1,20);
		if(pos == -1) {
			System.out.println("Elementul nu se afla in lista");
		}else {
			System.out.println("Elementul se afla pe pozitia " + pos);
		}
		
	}

}
