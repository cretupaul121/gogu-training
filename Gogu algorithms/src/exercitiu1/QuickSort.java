package exercitiu1;

public class QuickSort {
	
	
	public void quiqkSort(int[] arr, int low, int high) {
		
		if(low <= high) {
			int partition = partitioning(arr,low,high);
			quiqkSort(arr,low,partition-1);
			quiqkSort(arr,partition+1,high);
		}
	}
	
	

	
	private int partitioning(int[] arr, int low, int high) {
		int pivot = arr[low];
		int border = low + 1;
		int pivotLastPosition = 0;
		
		for(int i=border;i<=high;i++) {
			
			if(arr[i] < pivot) {
				swap(arr,i, border++);
			}
			
		}
		
		swap(arr,low,border-1);
		
		pivotLastPosition = border-1;
		
		return pivotLastPosition; 
	}
	
	private void swap(int[]arr, int a, int b) {
		int aux = arr[a];
		arr[a] = arr[b];
		arr[b] = aux;
	}
	
	public void print(int[]arr) {
		for(Integer i: arr) {
			System.out.println(i);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	int []arr = new int[] {20,19,28,16,15,13,26,16};	
		
	QuickSort test = new QuickSort();
	
	System.out.println("Inainte de sortare...");
	test.print(arr);
	
	test.quiqkSort(arr, 0, arr.length-1);
	
	System.out.println("Dupa sortare...");
	test.print(arr);
	
	}

}
