package exercitiu2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class TestSortarePersoane {
	
	public List<Person> lista;
	
	public TestSortarePersoane() {
		lista = new ArrayList<>();
	}
	
	public  void add(Person p) {
		lista.add(p);
	}
	
	public  void print() {
		for(Person p: lista) {
			System.out.println(p);
		}
	}
	
	public  void sort(Comparator<Person> comparator) {
		lista.sort(comparator);
	}
	
	public List<Person> getLista() {
		return lista;
	}
	
	
	public static void main(String[] args) {
		
		TestSortarePersoane test = new TestSortarePersoane();
		
		test.add(new Person("Paul",23,1.93f));	
		test.add(new Person("Gogu",22,1.85f));
		test.add(new Person("Nicoara",23,1.85f));
		test.add(new Person("Alexandra",22,1.78f));
		test.add(new Person("Stefan",22,1.85f));
		
		System.out.println("Sortare dupa nume:-------------------------");
		test.sort(new NameComparator());
		test.print();
		
		
		System.out.println("Sortare dupa varsta:-------------------------");
		test.sort(new AgeComparator());
		test.print();
		
		
		System.out.println("Sortare dupa inaltine:-------------------------");
		test.sort(new HeigthComparator());
		test.print();
		
		
	}
}
