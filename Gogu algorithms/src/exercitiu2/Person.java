package exercitiu2;

import java.util.Comparator;

public class Person {
	private String nume;
	private int varsta;
	private float inaltime;

	
	public Person(String nume, int varsta, float inaltime) {
		this.nume = nume;
		this.varsta = varsta;
		this.inaltime = inaltime;
	}


	public String getNume() {
		return nume;
	}


	public void setNume(String nume) {
		this.nume = nume;
	}


	public int getVarsta() {
		return varsta;
	}


	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}


	public float getInaltime() {
		return inaltime;
	}


	public void setInaltime(float inaltime) {
		this.inaltime = inaltime;
	}
	
	
	public String toString() {
		return "nume - " + nume + "\nvarsta - " + varsta + "\ninaltime - "+ inaltime+"\n";
	}
	
	
}

 class NameComparator implements Comparator<Person>{
	 
	 public int compare(Person p1, Person p2) {
		 
		 int nameResult = p1.getNume().compareTo(p2.getNume());
		 
		 return nameResult;
	 }
 }

 
 class AgeComparator implements Comparator<Person>{
	 
	 public int compare(Person p1, Person p2) {
		 
		 int ageResult = p1.getVarsta() - p2.getVarsta();
		 return ageResult;
	 }
 }
 
 
 class HeigthComparator implements Comparator<Person>{
	 
	 public int compare(Person p1, Person p2) {
		 int heigthResult = (int)p1.getInaltime() - (int)p2.getInaltime();
		 return heigthResult;
	 }
 }
 
 class CustomComparator implements Comparator<Person>{
	 
	 public int compare(Person p1, Person p2) {
		 	
		 int nameResult = p1.getNume().compareTo(p2.getNume());
		 int varstaResult = p1.getVarsta() - p2.getVarsta();
		 int heigthResult =(int)( p1.getInaltime() - p2.getInaltime());
		 
		 
		 if(nameResult == 0) {
			 if(varstaResult == 0) {
				 return heigthResult;
			 }
			return varstaResult;
		 }
		 
		 
		 return nameResult;
	 }
 }
