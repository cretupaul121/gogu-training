package exercitiu5;

import java.util.ArrayList;
import java.util.List;

public class TestInterclasare {

	public static ArrayList<Integer> mergedArray(List<Integer> list1, List<Integer> list2) {
		ArrayList<Integer> mergedList = new ArrayList<>();

		// caz lista 1 mai mica
		if (list1.size() < list2.size()) {

			for (int i = 0; i < list1.size(); i++) {

				int compare = list1.get(i) - list2.get(i);

				if (compare < 1) {
					mergedList.add(list1.get(i));
					mergedList.add(list2.get(i));
				} else {
					mergedList.add(list2.get(i));
					mergedList.add(list2.get(i));
				}

			}

			for (int i = list1.size(); i < list2.size(); i++) {

				mergedList.add(list2.get(i));

			}

		}

		// caz lista 1 este mai mare

		if (list1.size() > list2.size()) {

			for (int i = 0; i < list2.size(); i++) {

				int compare = list1.get(i) - list2.get(i);

				if (compare < 1) {
					mergedList.add(list1.get(i));
					mergedList.add(list2.get(i));
				} else {
					mergedList.add(list2.get(i));
					mergedList.add(list2.get(i));
				}

			}

			for (int i = list2.size(); i < list1.size(); i++) {

				mergedList.add(list1.get(i));
			}

		}
		
		
		//caz lista egale
		if(list1.size() == list2.size()) {
			
			for(int i=0;i<list1.size();i++) {
				
				int compare = list1.get(i) - list2.get(i);
				
				if(compare < 1) {
					mergedList.add(list1.get(i));
					mergedList.add(list2.get(i));
				}else {
					mergedList.add(list2.get(i));
					mergedList.add(list1.get(i));
				}
				
			}
			
			
		}
		

		return mergedList;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Integer> list1 = new ArrayList<>();
		List<Integer> list2 = new ArrayList<>();

		for (int i = 0; i < 5; i++) {
			list1.add(i + 3);
			list2.add(i + 4);
		}

		System.out.println("Lista1= ");
		for (Integer i : list1) {
			System.out.println(i);
		}
		
		

		System.out.println();
		System.out.println("Lista2 = ");
		for (Integer i : list2) {
			System.out.println(i);
		}

		List<Integer> merge = TestInterclasare.mergedArray(list1, list2);

		for (Integer i : merge) {
			System.out.print(i + " ");
		}

	}

}
