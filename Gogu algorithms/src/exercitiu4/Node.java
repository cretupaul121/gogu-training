package exercitiu4;

public class Node<K> {
	private Node<K> next;
	private K data;
	
	
	public Node() {
		
	}
	
	public Node(K data) {
		this.data = data;
	}
	
	
	public Node<K> getNextNode() {
		return next;
	}
	
	public void setNext(Node<K> next) {
		this.next = next;
	}
	
	public void setData(K data) {
		this.data = data;
	}
	
	public K getData() {
		return data;
	}
	
	
	public String toString() {
		return "data  = " + data;
	}
	
}
