package exercitiu4;

public class LinkedList<K> {
	private Node<K> head;
	private int size;
	
	public LinkedList() {
		head = null;
	}
	
	
	public Node<K> getHead(){
		return head;
	}
	
	public Node<K> addData(K data){
		
		Node<K> insertedNode = new Node<K>(data);
		Node<K> tempNode = head;
		
		if(head == null) {
			head = insertedNode;
		}else {
//			for(int i=0;i < size-1 && tempNode !=null; i++) {
//				tempNode = tempNode.getNextNode();
//			}
			
			while(tempNode.getNextNode() != null) {
				tempNode = tempNode.getNextNode();
			}
			
			tempNode.setNext(insertedNode);
					
		}
		
	
		size++;	
		
		return insertedNode;
	}
	
	
	public Node<K> deleteLastNode(){
		
		Node<K> node1 = head;
		//Node<K> node2 = head;
		
		if(head == null) {
			System.out.println("Empty list");
			return null;
		}
	
		// 1 	2	3
		
//		while(node1.getNextNode() != null) {
//			node2 = node1;
//			node1 = node1.getNextNode();
//		}
		
		//1		2	3	4	5	6	7
		
		for(int i=0;i<size-2;i++) {
			node1 = node1.getNextNode();
		}
			
		node1.setNext(null);
		size--;
		return node1;
	}
	
	
	public void addAt(K data, int pos){
		
		Node<K> insertedData = new Node<K>(data);
		Node<K> tempNode = head;
		
		if(pos > size) {
			System.out.println("The position is out of the size of the list");
			return ;
		}
		
		if(head == null) {
			System.out.println("The list is empty");
			return ;
		}
		
		if(pos == 0) {
			insertedData.setNext(tempNode.getNextNode());
			head = insertedData;
			size++;
		}
		
		//1		2	3	4	5	6	7
		
		
		for(int i=0; i<pos-1;i++) {
			tempNode = tempNode.getNextNode();
		}
		
		
		insertedData.setNext(tempNode.getNextNode());
		tempNode.setNext(insertedData);
				
		size++;
	}
	
	
	
	
	
	
	public void printLinkedList() {
		Node<K> tempNode = head;
		
		while(tempNode != null) {
			System.out.println(tempNode);
			tempNode = tempNode.getNextNode();
		}
		
	}
	
	
	public int getListSize() {
		return size;
	}
	
}
